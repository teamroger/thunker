exports.randomSelection = (items, size) => {
  const selection = [], length = items.length;
  let randomNumber, item;

  if (length < size) {
    return items;
  }

  while (selection.length < size) {
    randomNumber = Math.floor(Math.random() * length);
    item = items[randomNumber];

    if (selection.indexOf(item) === -1) {
      selection.push(item);
    }
  }

  return selection;
};
