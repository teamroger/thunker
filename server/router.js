const path = require('path');
const randomSelection = require(__dirname + '/utils.js').randomSelection;
const router = require('express').Router();
const getDB = require(__dirname + '/getDB.js');

router.get('/', function(req, res) {
  res.header('Content-Type', 'text/html');
  res.sendFile(__dirname + '/pages/index.html');
});

router.get('/js', function(req, res) {
  res.header('Content-Type', 'application/javascript');
  res.sendFile(path.resolve(__dirname + '/../dist/', 'main.js'));
});

router.get('/countries', function(req, res) {
  getDB().then((db) => {
    db.collection('countries').find({}).toArray(function(err, countries) {
      const selection = randomSelection(countries, 5);

      res.header('Content-Type', 'application/json');
      res.send(selection);
    });
  }).catch((err) => {
    console.log(err);
  });
});

module.exports = router;
