const MongoClient = require('mongodb').MongoClient;

const mongoURL = 'mongodb://rogerbarnfather:roger123@localhost:27017/thunker';

let promise;

function getDB() {
  if (!promise) {
    promise = new Promise((resolve, reject) => {
      MongoClient.connect(mongoURL, {useNewUrlParser: true}, (err, client) => {
        if (err) {
          reject(err);
        }

        const thunkerDB = client.db('thunker');
        resolve(thunkerDB);
      });
    });
  }

  return promise;
}

module.exports = getDB;
