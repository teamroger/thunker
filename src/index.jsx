import * as React from 'react';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import reducer from './app/reducer.js';
import { createStore, applyMiddleware } from 'redux';
import CountryListContainer from './components/CountryListContainer.jsx';

/*
  Create a store to contain the state and then
  pass it the reducer and the Thunk middleware
  to allow action creators to work asynchronously.
*/
const store = createStore(reducer, applyMiddleware(thunk));

/*
  Create a root React component, wrapped in a Provider to
  make the store available within that component.
*/
const providedComponent = (
  <Provider store={ store }>
    <CountryListContainer />
  </Provider>
);

/*
  Get the HTML element to insert our React component into.
*/
const item = document.getElementById('item');

/*
  Insert the React component into the HTML element
*/
ReactDOM.render(
  providedComponent,
  item
);
