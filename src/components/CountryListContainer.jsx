import * as React from 'react';
import { connect } from 'react-redux';
import CountryList from './CountryList.jsx';
import { updateCountries } from '../app/actionCreators.js';

/*
  Create a basic container component to pass certain props
  and modified functions to the "dumb" child component.
*/
const CountryListContainer = ({
  message,
  disableButton,
  countries,
  updateCountries
}) => (
  <CountryList
    message={ message }
    disableButton={ disableButton }
    countries={ countries }
    updateCountries={ updateCountries } />
)

/*
  Take the message and the list of countries from the state
  and add it to a new object representing the props data
  for the container above.
*/
function mapStateToProps({ message, disableButton, countries }) {
  return {
    message,
    disableButton,
    countries
  };
}

/*
  Take the dispatch function from the store and
  use it in a new object representing the props functions
  for the container above.
*/
function mapDispatchToProps(dispatch) {
  return {
    updateCountries:() => dispatch(updateCountries())
  }
}

/*
  Create a wrapped container component that receives
  the selection of props it needs and the wrapped
  functions that are required for dispatching actions.
*/
const connectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(CountryListContainer);

export default connectedComponent;
