/*
  Define an action creator to asynchronously update
  the list of countries, dispatching an action as it
  starts and another when it receives the data.
*/
export const updateCountries = () => {
  return (dispatch) => {
    dispatch(requestingCountries());

    fetch('/countries').then((response) => {
      return response.json();
    }).then((countries) => {
      dispatch(countriesReceived(countries));
    });
  };
};

/*
  Define an action creator to use when the request
  has been sent to retrieve a list of countries.
*/
const requestingCountries = () => ({
  type: 'REQUESTING_COUNTRIES'
});

/*
  Define an action creator to use when the request
  for a list of countries has been fulfilled.
*/
const countriesReceived = (countries) => ({
  type: 'COUNTRIES_RECEIVED',
  payload: countries
});
