import * as React from 'react';

/*
  Create a "dumb" component that displays part of the user
  interface based on the props passed to it and calls functions
  after events like button clicks.
*/
const CountryList = ({
  message,
  disableButton,
  updateCountries,
  countries
}) => (
  <div>
    <h3>List of countries</h3>

    { message.length > 0 &&
      <p>{ message }</p>
    }

    <button
      onClick={ updateCountries }
      disabled={ disableButton }
      >Update</button>

    { countries.length > 0 &&
      <ul> {
        countries.map((country) => (
          <li>{ country.name }</li>
        ))
      } </ul>
    }
  </div>
);

export default CountryList;
