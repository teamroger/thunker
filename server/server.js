const express = require('express');
const app = express();
const router = require(__dirname + '/router.js');
const httpPort = 3000;

// Everything in /public is available.
app.use(express.static(__dirname + '/public'));
app.use(router);

app.listen(httpPort, () => {
  console.log('Listening on port ' + httpPort + '...');
});
