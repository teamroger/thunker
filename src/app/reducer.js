/*
  Create an initial state for the reducer to use
  as a default before the application actually has
  a proper state.
*/
const initial = {
  message: 'There are currently no countries to show. Try updating.',
  disableButton: false,
  countries: []
};

/*
  Create a reducer to decide what the new state should be
  based on the old state and the incoming action.
*/
const reducer = (state = initial, action) => {
  switch (action.type) {
    case 'REQUESTING_COUNTRIES':
      return {
        ...state,
        message: 'Getting countries...',
        disableButton: true
      };

    case 'COUNTRIES_RECEIVED':
      return {
        message: 'Countries updated.',
        disableButton: false,
        countries: action.payload
      };

    default:
      return state;
  }
}

export default reducer;
