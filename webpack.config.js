const path = require('path');

module.exports = {
  // Entry point for the application tree
  entry: './src/index.jsx',

  // Output filename and directory for the bundled code
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },

  module: {
    rules: [
      // Rule number 1
      {
        // Look for .js or .jsx files
        test: /\.jsx?$/,

        // Do not look in node_modules directory
        exclude: /node_modules/,

        // Use the babel loader for .js and .jsx files
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};
