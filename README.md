# Thunker

## Playing around with asynchronous React & Redux

### Description
This is just a simple project to illustrate the creation and development of a React/Redux app that has asynchronous functionality.

### Setup
First ensure mongodb is installed on your system.

Then create a table/collection for the data:
$ mongo
$ mongo> use thunker
$ mongo> db.createCollection('countries')
$ mongo> db.countries.insert([{name: 'country1'}, {name: 'country2'}, ...etc])
$ mongo> db.countries.find() // Displays all countries
$ mongo> exit

Then create a mongo user in the terminal:
$ mongo
$ mongo> use thunker
$ mongo> db.createUser({'user':'rogerbarnfather', 'pwd':'roger123', 'roles':['readWrite','dbAdmin']})
$ mongo> exit
